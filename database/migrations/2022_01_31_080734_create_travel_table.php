<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTravelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('travel', function (Blueprint $table) {
            $table->id();
            $table->integer('company_id')->nullable();
            $table->integer('passenger_id')->nullable();
            $table->integer('travel_date_id')->nullable();
            $table->integer('picked_up_location_id')->nullable();
            $table->integer('status_id')->default(0); // 0 = Reservación, 1 = Aprobado, 2 = Cancelado, 3 = Rechazado
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('travel');
    }
}
