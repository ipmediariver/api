<?php

namespace Database\Factories;

use App\Models\Company;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class PassengerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id'    => User::factory(),
            'company_id' => Company::factory(),
            'phone'      => $this->faker->tollFreePhoneNumber(),
            'job_title'  => $this->faker->jobTitle,
            'gender'     => $this->faker->randomElement($array = ['male', 'female']),
            'fmm'        => rand(0, 1),
        ];
    }
}
