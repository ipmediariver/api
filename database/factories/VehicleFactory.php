<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class VehicleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {


        $type = $this->faker->word;
        $brand = $this->faker->word;
        $model = $this->faker->word;
        $year = rand(2018,2022); 
        $plates = hexdec(uniqid());

        return [
            'type' => $type,
            'brand' => $brand,
            'model' => $model,
            'year' => $year,
            'seats' => rand(5,15),
            'color' => $this->faker->randomElement($array = ['Black', 'White', 'Blue']),
            'plates' => hexdec(uniqid()),
            'name' => "{$brand} {$model} {$year} - {$plates}"
        ];
    }
}
