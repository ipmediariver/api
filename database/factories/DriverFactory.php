<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class DriverFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::factory()->create(['role' => 'driver']),
            'license_num' => hexdec(uniqid()),
            'avatar' => 'http://placehold.it/600x600',
            'gender' => $this->faker->randomElement($array = ['male', 'female'])
        ];
    }
}
