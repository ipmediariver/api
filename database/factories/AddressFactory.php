<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class AddressFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        $street = $this->faker->streetAddress;
        $street_2 = $this->faker->streetAddress;
        $city = $this->faker->city;
        $state = $this->faker->state;
        $country = $this->faker->country;
        $zip = rand(22000,22500);

        return [
            'street' => $street,
            'street_2' => $street_2,
            'city' => $city,
            'state' => $state,
            'country' => $country,
            'zip_code' => $zip,
            'alias' => "{$street}, {$city} {$state}, {$country}, {$zip}"
        ];
    }
}
