<?php

namespace Database\Factories;

use Carbon\Carbon;
use App\Models\Driver;
use App\Models\Vehicle;
use Illuminate\Database\Eloquent\Factories\Factory;

class TravelDateFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $day = Carbon::today()->addDays(rand(0,7));
        $time = Carbon::parse($day)->addHours(rand(0,5));
        return [
            'date' => $day,
            'start_at' => $time,
            'departure' => $this->faker->city,
            'destination' => $this->faker->city,
            'vehicle_id' => Vehicle::factory(),
            'driver_id'=> Driver::factory()
        ];
    }
}
