<?php

namespace Database\Factories;

use App\Models\Company;
use App\Models\Passenger;
use App\Models\TravelDate;
use Illuminate\Database\Eloquent\Factories\Factory;

class TravelFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'company_id' => Company::factory(),
            'passenger_id' => Passenger::factory(),
            'travel_date_id' => TravelDate::factory(),
            'status_id' => 0
        ];
    }
}
