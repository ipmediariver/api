<?php

namespace Database\Seeders;

use App\Models\Travel;
use App\Models\PickedUpLocation;
use Illuminate\Database\Seeder;

class TravelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Travel::factory()->count(10)->create()->each(function($travel){
            
            $pickedUpLocations = $this->setPickedUpLocations($travel);
            $travel->picked_up_location_id = $pickedUpLocations->random(1)->first()->id;
            $travel->save();

        });
    }

    public function setPickedUpLocations($travel){
        $travelDate = $travel->date;
        return PickedUpLocation::factory()->count(rand(1,3))->create([
            'travel_date_id' => $travelDate->id 
        ]);
    }

}
