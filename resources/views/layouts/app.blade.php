<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
    </head>
    <body class="font-sans antialiased">
        <div class="min-h-screen bg-gray-100">
            @include('layouts.navigation')

            <!-- Page Heading -->
            <header class="bg-primary shadow z-10 relative">
                <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                    <h2 class="font-semibold text-xl text-white leading-tight">
                        {{ $header }}
                    </h2>
                </div>
            </header>

            <!-- Page Content -->
            <main>
                @if (session('status'))
                    <div class="bg-green-500 py-5 px-7 text-white font-bold">
                        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                            {{ session('status') }}
                        </div>
                    </div>
                @endif
                <div class="py-12">
                    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
                        {{ $body }}
                    </div>
                </div>
            </main>

            <footer class="py-10 text-center">
                <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                    ©{{ date('Y') }}, {{ env('APP_NAME') }}, Rights Reserved.
                </div>
            </footer>
        </div>
    </body>
</html>
