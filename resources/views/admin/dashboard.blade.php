<x-app-layout header="Welcome {{ auth()->user()->name }}">
	<x-slot name="body">
		<x-admin.travels-list :travels="$currentTravels" title="Next travels" />
	</x-slot>
</x-app-layout>