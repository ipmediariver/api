<x-app-layout header="New User">
	<x-slot name="body">
		<div class="w-7/12 mx-auto">
			<div class="bg-white shadow-md rounded overflow-hidden">
				<div class="p-5">
					<x-admin.user-form title="New User" method="POST" route="{{ route('store-user') }}" />
				</div>
			</div>
		</div>
	</x-slot>
</x-app-layout>