<x-app-layout header="Users List">
	<x-slot name="body">
		<x-admin.users-list :users="$users" title="Users List" />
	</x-slot>
</x-app-layout>