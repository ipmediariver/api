<x-app-layout header="Edit User">
	<x-slot name="body">
		<div class="w-7/12 mx-auto">
			<div class="bg-white shadow-md rounded overflow-hidden">
				<div class="p-5">
					<x-admin.user-form :user="$user" title="Edit User" method="PATCH" route="{{ route('update-user', $user) }}" />
				</div>
			</div>

			<hr class="my-7 border-2 border-gray-400">

			<form action="{{route('delete-user', $user)}}" method="POST">
			    @csrf
			    @method('DELETE')
			    <x-button type="submit" class="bg-red-400 hover:bg-red-500" onclick="return confirm('Are you sure you want to delete this user?')">
			        Delete user
			    </x-button>
			</form>
		</div>
	</x-slot>
</x-app-layout>