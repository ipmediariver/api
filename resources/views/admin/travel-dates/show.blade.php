<x-app-layout header="Travel Date Details">
	<x-slot name="body">
		<div class="w-7/12 mx-auto">
			<x-admin.travel-date-details
				:travelDate="$travelDate" 
				route="{{ route('update-travel-date', $travelDate) }}"
				method="PATCH" />
		</div>
	</x-slot>
</x-app-layout>