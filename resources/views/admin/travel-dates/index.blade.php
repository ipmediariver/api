<x-app-layout header="Travel Dates List">
	<x-slot name="body">
		<x-admin.travel-dates-list :travelDates="$travelDates" title="Travel Dates List" />
	</x-slot>
</x-app-layout>