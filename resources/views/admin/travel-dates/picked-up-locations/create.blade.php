<x-app-layout header="New picked up location">
	<x-slot name="body">
		<div class="w-7/12 mx-auto">
			<x-admin.picked-up-location-form :travelDate="$travelDate" />
		</div>
	</x-slot>
</x-app-layout>