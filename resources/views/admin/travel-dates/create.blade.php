<x-app-layout header="Travel Dates List">
	<x-slot name="body">
		<div class="w-7/12 mx-auto">
			<div class="bg-white shadow-md rounded overflow-hidden">
				<div class="p-5">
					<x-admin.travel-date-form 
						title="New Travel Date"
						route="{{ route('store-travel-date') }}"
						method="POST" />
				</div>
			</div>
		</div>
	</x-slot>
</x-app-layout>