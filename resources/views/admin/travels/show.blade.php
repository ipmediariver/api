<x-app-layout header="Travel Details">
	<x-slot name="body">
		<div class="w-7/12 mx-auto">
			<x-admin.travel-form 
				:travel="$travel" 
				route="{{ route('update-travel', $travel->id) }}"
				method="PUT" />
		</div>
	</x-slot>
</x-app-layout>