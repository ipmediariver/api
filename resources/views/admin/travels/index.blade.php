<x-app-layout header="Travels List">
	<x-slot name="body">
		<x-admin.travels-list :travels="$travels" title="Travels list" />
	</x-slot>
</x-app-layout>