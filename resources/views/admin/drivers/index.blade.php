<x-app-layout header="Drivers List">
	<x-slot name="body">
		<x-admin.drivers-list :drivers="$drivers" title="Drivers List" />
	</x-slot>
</x-app-layout>