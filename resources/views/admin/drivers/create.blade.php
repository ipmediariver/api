<x-app-layout header="New Driver">
	<x-slot name="body">
		<div class="w-7/12 mx-auto">
			<div class="bg-white shadow-md rounded overflow-hidden">
				<div class="p-5">
					<x-admin.driver-form title="New Driver" method="POST" route="{{ route('store-driver') }}" />
				</div>
			</div>
		</div>
	</x-slot>
</x-app-layout>