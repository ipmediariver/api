<x-app-layout header="Edit Driver">
	<x-slot name="body">
		<div class="w-7/12 mx-auto">
			<div class="bg-white shadow-md rounded overflow-hidden">
				<div class="p-5">
					<x-admin.driver-form :driver="$driver" title="Edit Driver" method="PATCH" route="{{ route('update-driver', $driver) }}" />
				</div>
			</div>

			<hr class="my-7 border-2 border-gray-400">

			<form action="{{route('delete-driver', $driver)}}" method="POST">
			    @csrf
			    @method('DELETE')
			    <x-button type="submit" class="bg-red-400 hover:bg-red-500" onclick="return confirm('Are you sure you want to delete this driver?')">
			        Delete driver
			    </x-button>
			</form>
		</div>
	</x-slot>
</x-app-layout>