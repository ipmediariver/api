<x-app-layout header="Vehicle Details">
	<x-slot name="body">
		<div class="w-7/12 mx-auto">
			<div class="bg-white shadow-md rounded overflow-hidden">
				<div class="p-5">
					<x-admin.vehicle-form 
						title="Edit Vehicle" 
						method="PATCH" 
						route="{{ route('update-vehicle', $vehicle) }}"
						:vehicle="$vehicle" />
				</div>
			</div>

			<hr class="my-7 border-2 border-gray-400">

			<form action="{{route('delete-vehicle', $vehicle)}}" method="POST">
			    @csrf
			    @method('DELETE')
			    <x-button type="submit" class="bg-red-400 hover:bg-red-500" onclick="return confirm('Are you sure you want to delete this vehicle?')">
			        Delete vehicle
			    </x-button>
			</form>

		</div>
	</x-slot>
</x-app-layout>