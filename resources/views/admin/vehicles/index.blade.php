<x-app-layout header="Vehicles List">
	<x-slot name="body">
		<x-admin.vehicles-list :vehicles="$vehicles" title="Vehicles List" />
	</x-slot>
</x-app-layout>