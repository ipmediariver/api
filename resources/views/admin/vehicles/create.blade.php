<x-app-layout header="New Vehicle">
	<x-slot name="body">
		<div class="w-7/12 mx-auto">
			<div class="bg-white shadow-md rounded overflow-hidden">
				<div class="p-5">
					<x-admin.vehicle-form title="New Vehicle" method="POST" route="{{ route('store-vehicle') }}" />
				</div>
			</div>
		</div>
	</x-slot>
</x-app-layout>