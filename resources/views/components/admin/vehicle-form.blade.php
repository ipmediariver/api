<form action="{{ $route }}" method="POST">
    @csrf
    @if($method != 'POST')
        @method($method)
    @endif

    <div class="flex items-center mb-5">
        <h3 class="font-bold text-xl">{{ $title }}</h3>
        <x-button type="submit" class="ml-auto">Save</x-button>
    </div>

    <div class="mb-3">
        <label for="" class="mb-2">Type:</label>
        <input type="text" class="w-full" name="type" value="{{ $vehicle ? $vehicle->type : old('type') }}" required>
    </div>

    <div class="mb-3">
        <label for="" class="mb-2">Brand:</label>
        <input type="text" class="w-full" name="brand"  value="{{ $vehicle ? $vehicle->brand : old('brand') }}" required>
    </div>

    <div class="mb-3">
        <label for="" class="mb-2">Model:</label>
        <input type="text" class="w-full" name="model"  value="{{ $vehicle ? $vehicle->model : old('model') }}" required>
    </div>

    <div class="flex items-center space-x-3 mb-3 w-full">
        <div class="flex-1">
            <label for="" class="mb-2">Year:</label>
            <input type="number" class="w-full" name="year"  value="{{ $vehicle ? $vehicle->year : old('year') }}" required>
        </div>

        <div class="flex-1">
            <label for="" class="mb-2">Plates:</label>
            <input type="text" class="w-full" name="plates"  value="{{ $vehicle ? $vehicle->plates : old('plates') }}" required>
        </div>
    </div>

    <div class="flex items-center space-x-3 w-full">
        <div class="flex-1">
            <label for="" class="mb-2">Color:</label>
            <input type="text" class="w-full" name="color"  value="{{ $vehicle ? $vehicle->color : old('color') }}" required>
        </div>

        <div class="flex-1">
            <label for="" class="mb-2">Seats:</label>
            <input type="number" class="w-full" name="seats"  value="{{ $vehicle ? $vehicle->seats : old('seats') }}" required>
        </div>
    </div>
    
</form>