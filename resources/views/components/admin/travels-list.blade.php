<div class="bg-white p-5 rounded shadow-md">
    <div class="flex itemx-center mb-7">
        <h3 class="font-bold text-xl">{{ $title }}</h3>
    </div>
    <table class="w-full text-left">
        <thead>
            <tr>
                <th class="py-3 border-b border-gray-300">Passenger</th>
                <th class="py-3 border-b border-gray-300">Date</th>
                <th class="py-3 border-b border-gray-300">Hour</th>
                <th class="py-3 border-b border-gray-300">Departure</th>
                <th class="py-3 border-b border-gray-300">Destination</th>
                <th class="py-3 border-b border-gray-300">Status</th>
                <th class="py-3 border-b border-gray-300 text-right">Details</th>
            </tr>
        </thead>

        <tbody>
            @foreach($travels as $travel)
            <tr>
                <td class="border-b border-gray-300">{{ $travel->passenger }}</td>
                <td class="border-b border-gray-300">{{ $travel->day }}</td>
                <td class="border-b border-gray-300">{{ $travel->hour}}</td>
                <td class="border-b border-gray-300">{{ $travel->departure}}</td>
                <td class="border-b border-gray-300">{{ $travel->destination }}</td>
                <td class="border-b border-gray-300">{{ $travel->status }}</td>
                <td class="border-b border-gray-300 text-right">
                    <a href="{{ route('show-travel', $travel->id) }}" class="text-indigo-400">
                        Details
                    </a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>