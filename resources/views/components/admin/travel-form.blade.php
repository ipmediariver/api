<div class="bg-white shadow-md rounded overflow-hidden">
    <form action="{{ $route }}" method="POST">
        @csrf

        @if($method == 'PUT')
            @method('PUT')
        @endif

        <div class="p-5">
            <div class="flex items-end mb-3">
                <div>
                    <label class="block mb-2" for="">Status:</label>
                    <select name="status_id" id="">
                        <option disabled selected value="">Choose an option</option>
                        <option @if($status == 0) selected @endif value="0">Pending approval</option>
                        <option @if($status == 1) selected @endif value="1">Approved</option>
                        <option @if($status == 2) selected @endif value="2">Cancelled</option>
                        <option @if($status == 3) selected @endif value="3">Rejected</option>
                    </select>
                </div>

                <x-button type="submit" class="ml-auto" 
                    onclick="return confirm('Are you sure you want to proceed?')">
                    Save changes
                </x-button>
            </div>

            <hr class="my-5">

            <p class="text-sm mb-5 font-bold text-gray-600">Reserved: {{ $createdAt }}</p>

            <div>
                <label class="block mb-2" for="">Passenger:</label>
                <input type="text" class="bg-gray-100 text-gray-500 w-full" disabled value="{{ $passenger }}">
            </div>
        </div>

        <div class="rounded bg-white p-5 border-t border-gray-200">

            <div class="mb-4">
                <a href="{{ route('show-travel-date', $travel->travel_date_id) }}" class="font-bold text-sm text-indigo-600 underline">
                    View travel date details
                </a>
            </div>

            <div class="flex items-center space-x-5 mb-3">
                <div class="flex-1">
                    <label class="block mb-2" for="">Date:</label>
                    <input type="text" disabled class="w-full bg-gray-100 text-gray-500" value="{{ $day }}">
                </div>

                <div class="flex-1">
                    <label class="block mb-2" for="">Hour:</label>
                    <input type="text" disabled class="w-full bg-gray-100 text-gray-500" value="{{ $hour }}">
                </div>
            </div>

            <div class="flex items-center space-x-5">
                <div class="mb-3 flex-1">
                    <label class="block mb-2" for="">Departure:</label>
                    <input type="text" disabled class="w-full bg-gray-100 text-gray-500" value="{{ $departure }}">
                </div>

                <div class="mb-3 flex-1">
                    <label class="block mb-2" for="">Destination:</label>
                    <input type="text" disabled class="w-full bg-gray-100 text-gray-500" value="{{ $destination }}">
                </div>
            </div>

            <div class="mb-3">
                <label class="block mb-2" for="">Picked up at:</label>
                <input type="text" disabled class="w-full bg-gray-100 text-gray-500" value="{{ $pickedUp }}">
            </div>
        </div>
    </form>
</div>