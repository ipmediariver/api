<div class="bg-white p-5 rounded shadow-md">
    <div class="flex itemx-center mb-7">
        <h3 class="font-bold text-xl">{{ $title }}</h3>
        <x-link href="{{ route('create-driver') }}" 
            class="ml-auto" 
            type="submit">
            New
        </x-link>
    </div>
    <table class="w-full text-left">
        <thead>
            <tr>
                <th class="py-3 border-b border-gray-300">Name</th>
                <th class="py-3 border-b border-gray-300">E-mail</th>
                <th class="py-3 border-b border-gray-300">license_num</th>
                <th class="py-3 border-b border-gray-300">gender</th>
                <th class="py-3 border-b border-gray-300 text-right">Details</th>
            </tr>
        </thead>

        <tbody>
            @foreach($drivers as $driver)
            <tr>
                <td class="border-b border-gray-300">{{ $driver->user->name }}</td>
                <td class="border-b border-gray-300">{{ $driver->user->email }}</td>
                <td class="border-b border-gray-300">{{ $driver->license_num }}</td>
                <td class="border-b border-gray-300 capitalize">{{ $driver->gender }}</td>
                <td class="border-b border-gray-300 text-right">
                    <a href="{{ route('show-driver', $driver->id) }}" class="text-indigo-400">
                        Details
                    </a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>