<form action="{{ $route }}" class="flex flex-col space-y-3" method="POST">
    @csrf
    @if($method != 'POST')
        @method('PATCH')
    @endif
    <div class="flex items-center mb-5">
        <h3 class="font-bold text-xl">{{ $title }}</h3>
        <x-button type="submit" class="ml-auto">Save</x-button>
    </div>

    <div class="flex-1">
        <label class="block mb-2" for="">Date:</label>
        <input type="date" name="date" class="w-full text-gray-500" value="{{ $date }}">
    </div>
    <div class="flex-1">
        <label class="block mb-2" for="">Hour:</label>
        <input type="time" name="hour" class="w-full text-gray-500" value="{{ $hour }}">
    </div>
    <div class="flex-1">
        <label class="block mb-2" for="">Departure:</label>
        <input type="text" name="departure" class="w-full text-gray-500" value="{{ $departure }}">
    </div>
    <div class="flex-1">
        <label class="block mb-2" for="">Destination:</label>
        <input type="text" name="destination" class="w-full text-gray-500" value="{{ $destination }}">
    </div>
</form>