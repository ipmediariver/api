<div class="mb-5">
    <a class="font-bold" href="{{ route('show-travel-date', $travelDate) }}">
        < Back
    </a>
</div>
<div class="bg-white shadow-md rounded overflow-hidden">
    <form action="{{ route('store-picked-up-location', $travelDate) }}" method="POST" class="p-5">
        @csrf
        <div class="flex items-center mb-5">
            <p class="font-bold">Select an address</p>
            <x-button type="submit" class="ml-auto">Save</x-button>
        </div>
        <div>
            <label for="">Address:</label>
            <select name="address_id" id="" class="w-full" required>
                <option value="">Choose an option</option>
                @foreach($addresses as $address)
                <option value="{{ $address->id }}">{{ $address->alias }}</option>
                @endforeach
            </select>
        </div>
    </form>
</div>
<div class="bg-white shadow-md rounded overflow-hidden mt-4">
    <form action="{{ route('store-picked-up-location', $travelDate) }}" class="p-5" method="POST">
        @csrf
        <div class="flex items-center mb-5">
            <p class="font-bold">Or add a new one</p>
            <x-button type="submit" class="ml-auto">Save</x-button>
        </div>
        <div class="mb-3">
            <label for="">Street:</label>
            <input type="text" name="street" class="w-full" required>
        </div>
        <div class="mb-3">
            <label for="">City:</label>
            <input type="text" name="city" class="w-full" required>
        </div>
        <div class="mb-3">
            <label for="">State:</label>
            <input type="text" name="state" class="w-full" required>
        </div>
        <div class="mb-3">
            <label for="">Country:</label>
            <input type="text" name="country" class="w-full" required>
        </div>
        <div class="mb-3">
            <label for="">Zip code:</label>
            <input type="text" name="zip" class="w-full" required>
        </div>
    </form>
</div>