<form action="{{ $route }}" method="POST">
    @csrf
    @if($method != 'POST')
        @method($method)
    @endif

    <div class="flex items-center mb-5">
        <h3 class="font-bold text-xl">{{ $title }}</h3>
        <x-button type="submit" class="ml-auto">Save</x-button>
    </div>

    <div class="mb-3">
        <label for="" class="mb-2">Name:</label>
        <input type="text" class="w-full" name="name" value="{{ $driver ? $driver->user->name : old('name') }}" required>
    </div>

    <div class="mb-3">
        <label for="" class="mb-2">E-mail:</label>
        <input type="email" class="w-full" name="email" value="{{ $driver ? $driver->user->email : old('email') }}" required>
    </div>

    <div class="mb-3">
        <label for="" class="mb-2">License Number:</label>
        <input type="text" class="w-full" name="license_num" value="{{ $driver ? $driver->license_num : old('license_num') }}" required>
    </div>

    <div class="mb-3">
        <label for="" class="mb-2">Gender:</label>
        <select name="gender" id="" class="w-full">
            <option disabled selected value="">Choose an option</option>
            <option @if($driver) @if($driver->gender == 'male') selected @endif @endif value="male">Male</option>
            <option @if($driver) @if($driver->gender == 'female') selected @endif @endif value="female">Female</option>
        </select>
    </div>

    <hr class="my-5">

    <p class="font-bold mb-4">Security</p>

    <div class="mb-3">
        <label for="" class="mb-2">Password:</label>
        <input type="password" class="w-full" name="password" @if($method == 'POST') required @endif>
    </div>    
    
</form>