<div class="bg-white p-5 rounded shadow-md">
    <div class="flex itemx-center mb-7">
        <h3 class="font-bold text-xl">{{ $title }}</h3>
        <x-link href="{{ route('create-travel-date') }}" 
            class="ml-auto" 
            type="submit">
            New
        </x-link>
    </div>
    <table class="w-full text-left">
        <thead>
            <tr>
                <th class="py-3 border-b border-gray-300">Date</th>
                <th class="py-3 border-b border-gray-300">Hour</th>
                <th class="py-3 border-b border-gray-300">Departure</th>
                <th class="py-3 border-b border-gray-300">Destination</th>
                <th class="py-3 border-b border-gray-300 text-right">Details</th>
            </tr>
        </thead>

        <tbody>
            @foreach($travelDates as $travelDate)
            <tr>
                <td class="border-b border-gray-300">{{ $travelDate->date }}</td>
                <td class="border-b border-gray-300">{{ $travelDate->hour }}</td>
                <td class="border-b border-gray-300">{{ $travelDate->departure }}</td>
                <td class="border-b border-gray-300">{{ $travelDate->destination }}</td>
                <td class="border-b border-gray-300 text-right">
                    <a href="{{ route('show-travel-date', $travelDate->id) }}" class="text-indigo-400">
                        Details
                    </a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>