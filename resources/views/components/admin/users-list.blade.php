<div class="bg-white p-5 rounded shadow-md">
    <div class="flex itemx-center mb-7">
        <h3 class="font-bold text-xl">{{ $title }}</h3>
        <x-link href="{{ route('create-user') }}" 
            class="ml-auto" 
            type="submit">
            New
        </x-link>
    </div>
    <table class="w-full text-left">
        <thead>
            <tr>
                <th class="py-3 border-b border-gray-300">Name</th>
                <th class="py-3 border-b border-gray-300">E-mail</th>
                <th class="py-3 border-b border-gray-300">Role</th>
                <th class="py-3 border-b border-gray-300 text-right">Details</th>
            </tr>
        </thead>

        <tbody>
            @foreach($users as $user)
            <tr>
                <td class="border-b border-gray-300">{{ $user->name }}</td>
                <td class="border-b border-gray-300">{{ $user->email }}</td>
                <td class="border-b border-gray-300">{{ $user->role }}</td>
                <td class="border-b border-gray-300 text-right">
                    <a href="{{ route('show-user', $user->id) }}" class="text-indigo-400">
                        Details
                    </a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>