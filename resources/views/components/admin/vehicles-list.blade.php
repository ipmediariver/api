<div class="bg-white p-5 rounded shadow-md">
    <div class="flex itemx-center mb-7">
        <h3 class="font-bold text-xl">{{ $title }}</h3>
        <x-link href="{{ route('create-vehicle') }}" 
            class="ml-auto" 
            type="submit">
            New
        </x-link>
    </div>
    <table class="w-full text-left">
        <thead>
            <tr>
                <th class="py-3 border-b border-gray-300">Type</th>
                <th class="py-3 border-b border-gray-300">Brand</th>
                <th class="py-3 border-b border-gray-300">Model</th>
                <th class="py-3 border-b border-gray-300">Year</th>
                <th class="py-3 border-b border-gray-300">Plates</th>
                <th class="py-3 border-b border-gray-300">Color</th>
                <th class="py-3 border-b border-gray-300">Seats</th>
                <th class="py-3 border-b border-gray-300 text-right">Details</th>
            </tr>
        </thead>

        <tbody>
            @foreach($vehicles as $vehicle)
            <tr>
                <td class="border-b border-gray-300">{{ $vehicle->type }}</td>
                <td class="border-b border-gray-300">{{ $vehicle->brand }}</td>
                <td class="border-b border-gray-300">{{ $vehicle->model }}</td>
                <td class="border-b border-gray-300">{{ $vehicle->year }}</td>
                <td class="border-b border-gray-300">{{ $vehicle->plates }}</td>
                <td class="border-b border-gray-300">{{ $vehicle->color }}</td>
                <td class="border-b border-gray-300">{{ $vehicle->seats }}</td>
                <td class="border-b border-gray-300 text-right">
                    <a href="{{ route('show-vehicle', $vehicle->id) }}" class="text-indigo-400">
                        Details
                    </a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>