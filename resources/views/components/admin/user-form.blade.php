<form action="{{ $route }}" method="POST">
    @csrf
    @if($method != 'POST')
        @method($method)
    @endif

    <div class="flex items-center mb-5">
        <h3 class="font-bold text-xl">{{ $title }}</h3>
        <x-button type="submit" class="ml-auto">Save</x-button>
    </div>

    <div class="mb-3">
        <label for="" class="mb-2">Name:</label>
        <input type="text" class="w-full" name="name" value="{{ $user ? $user->name : old('name') }}" required>
    </div>

    <div class="mb-3">
        <label for="" class="mb-2">E-mail:</label>
        <input type="email" class="w-full" name="email" value="{{ $user ? $user->email : old('email') }}" required>
    </div>

    <div class="mb-3">
        <label for="" class="mb-2">Role:</label>
        <select name="role" class="w-full" id="">
            <option selected disabled value="">Choose an option</option>
            <option @if($user) @if($user->role == 'admin') selected @endif @endif value="admin">Administrator</option>
            <option @if($user) @if($user->role == 'driver') selected @endif @endif value="driver">Driver</option>
            <option @if($user) @if($user->role == 'passenger') selected @endif @endif value="passenger">Passenger</option>
        </select>
    </div>

    <hr class="my-5">

    <p class="font-bold mb-4">Security</p>

    <div class="mb-3">
        <label for="" class="mb-2">Password:</label>
        <input type="password" class="w-full" name="password" @if($method == 'POST') required @endif>
    </div>    
    
</form>