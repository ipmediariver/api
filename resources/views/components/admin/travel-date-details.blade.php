<div class="bg-white shadow-md rounded overflow-hidden">
    <div class="p-5">
        <x-admin.travel-date-form
            :method="$method"
            title="Travel Date Details"
            :travelDate="$travelDate" />
    </div>
</div>

<div class="bg-white shadow-md rounded overflow-hidden mt-5">
    <div class="p-5">
        <div class="mb-5">
            <h3 class="font-bold text-xl">Picked up locations</h3>
        </div>
        @foreach($travelDate->pickedUpLocations as $location)
        <div class="flex items-start border-b border-gray-300">
            <p class="mr-5">
                {{ $location->address->alias }}
            </p>
            <form action="{{ route('delete-picked-up-location', [$travelDate, $location]) }}" method="POST" class="ml-auto">
                @csrf
                @method('DELETE')
                <button type="submit" 
                    onclick="return confirm('Are you sure to delete this location?')"
                    class="text-red-600 underline hover:text-red-500">
                    Delete
                </button>
            </form>
        </div>
        @endforeach
        <div class="mt-4">
            <a href="{{ route('create-picked-up-location', [$travelDate]) }}" class="font-bold text-sm text-indigo-600 underline">Add new picked up address</a>
        </div>
    </div>
</div>

<div class="bg-white shadow-md rounded overflow-hidden mt-5">
    <form action="{{ route('add-vehicle-and-driver-to-travel-date', $travelDate) }}" class="p-5" method="POST">
        @csrf
        <div class="mb-5 flex items-center">
            <h3 class="font-bold text-xl mb-5">Vehicle and driver</h3>
            <x-button class="ml-auto" type="submit">Save</x-button>
        </div>
        <div class="mb-3">
            <label for="" class="mb-2">Vehicle</label>
            <select name="vehicle_id" id="" class="w-full" required>
                <option disabled selected value="">Choose an option</option>
                @foreach($vehicles as $vehicle)
                <option @if($travelDate->vehicle_id == $vehicle->id) selected @endif value="{{ $vehicle->id }}">{{ $vehicle->name }}</option>
                @endforeach
            </select>
        </div>
        <div>
            <label for="" class="mb-2">Driver</label>
            <select name="driver_id" id="" class="w-full" required>
                <option disabled selected value="">Choose an option</option>
                @foreach($drivers as $driver)
                <option @if($travelDate->driver_id == $driver->id) selected @endif value="{{ $driver->id }}">{{ $driver->user->name }}</option>
                @endforeach
            </select>
        </div>
    </form>
</div>

<hr class="my-7 border-2 border-gray-400">

<form action="{{route('delete-travel-date', $travelDate)}}" method="POST">
    @csrf
    @method('DELETE')
    <x-button type="submit" class="bg-red-400 hover:bg-red-500" onclick="return confirm('Are you sure you want to delete this travel date?')">
        Delete travel date
    </x-button>
</form>