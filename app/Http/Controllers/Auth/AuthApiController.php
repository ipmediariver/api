<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthApiController extends Controller
{
    public function login(Request $request){
        $rules = $this->loginValidationRules();
        Validator::make($request->all(), $rules)->validate();

        $user = User::where('email', $request->email)->first();

        if(!$user){
            $response = ["message" => "These credentials do not match our records."];
            return response($response, 401);
        }

        if(!Hash::check($request->password, $user->password)){
            $response = ["message" => "These credentials do not match our records."];
            return response($response, 401);
        }

        $token = $user->createToken('Personal Access Token')->accessToken;
        $response = ['token' => $token];
        return response($response, 200);

    }

    public function logout(Request $request){
        $user = request()->user();
        foreach($user->tokens as $token){
            $token->delete();
        }
        $response = ['message' => 'User is logout'];
        return response($response, 200);
    }

    public function loginValidationRules(){
        return [
            'email' => 'required|email|exists:users',
            'password' => 'required'
        ];
    }

    public function user(Request $request){
        $user = $request->user();
        $initials = collect(explode(' ', $user->name))
            ->map(function($item){ return $item[0]; })
            ->join('+');
        $user->avatar = "https://ui-avatars.com/api/?name={$initials}&background=235471&color=fff";
        return $user;
    }
}
