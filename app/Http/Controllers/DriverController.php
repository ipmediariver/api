<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Driver;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\StoreDriverRequest;
use App\Http\Requests\UpdateDriverRequest;

class DriverController extends Controller
{

    public function __construct()
    {
        $this->middleware('role:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $drivers = DB::table('drivers')
            ->join('users', 'drivers.user_id', 'users.id')
            ->select([
                'users.name as name',
                'users.email as email',
                'drivers.license_num as license',
                'drivers.avatar as avatar',
                'drivers.gender as gender',
            ])->get();

        return response($drivers, 200);
            
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreDriverRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDriverRequest $request)
    {

        $user = $this->setDriverUser($request);
        $driver = $this->setDriver($request, $user);

        $response = ["user" => $user, "driver" => $driver];
        return response($response, 200);

    }

    public function setDriverUser($request){
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->role = 'driver';
        $user->save();
        return $user;
    }

    public function setDriver($request, $user){
        $driver = new Driver();
        $driver->user_id = $user->id;
        $driver->license_num = $request->license_num;
        $driver->gender = $request->gender;
        $driver->save();
        return $driver;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function show(Driver $driver)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function edit(Driver $driver)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateDriverRequest  $request
     * @param  \App\Models\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDriverRequest $request, Driver $driver)
    {
        try {
            $this->updateDriverUser($request, $driver);
            $this->updateDriver($request, $driver);
            $response = ["message" => "Driver information has been updated."];
            return response($response, 200);
        } catch (\Throwable $th) {
            $error = ["message" => "Something were wrong, please try again later."];
            return response($error, 500);
        }
    }

    public function updateDriverUser($request, $driver){
        $user = $driver->user;
        $user->name = $request->name;
        $user->email = $request->email;
        if($request->has('password')){
            $user->password = Hash::make($request->password);
        }
        $user->save();
    }

    public function updateDriver($request, $driver){
        $driver->license_num = $request->license_num;
        $driver->gender = $request->gender;
        $driver->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Driver  $driver
     * @return \Illuminate\Http\Response
     */
    public function destroy(Driver $driver)
    {
        try {
            $driver->delete();
            $response = ["message" => "The driver has been removed."];
            return response($response, 200);
        } catch (\Throwable $th) {
            $error = ["message" => "Something were wrong, please try again later."];
            return response($error, 500);
        }
    }
}
