<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Passenger;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\StorePassengerRequest;
use App\Http\Requests\UpdatePassengerRequest;

class PassengerController extends Controller
{

    public function __construct()
    {
        $this->middleware('role:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $passengers = DB::table('passengers')
            ->join('companies', 'passengers.company_id', 'companies.id')
            ->join('users', 'passengers.user_id', 'users.id')
            ->select([
                'passengers.id as id',
                'companies.name as company_name',
                'users.name as name',
                'users.email as email',
                'passengers.phone as phone',
                'passengers.job_title as job_title',
                'passengers.gender as gender',
                'passengers.fmm as fmm'
            ])
            ->get();

        return response($passengers, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorePassengerRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePassengerRequest $request)
    {
        $newUser = $this->setPassengerUser($request);
        $newPassenger = $this->setPassenger($request, $newUser);
        $passenger = $this->getPassengerInfo($newPassenger->id);
        return response($passenger, 200);
    }

    public function getPassengerInfo($id){
        $passenger = DB::table('passengers')
            ->where('passengers.id', $id)
            ->join('companies', 'passengers.company_id', 'companies.id')
            ->join('users', 'passengers.user_id', 'users.id')
            ->select([
                'passengers.id as id',
                'companies.name as company_name',
                'users.name as name',
                'users.email as email',
                'passengers.phone as phone',
                'passengers.job_title as job_title',
                'passengers.gender as gender',
                'passengers.fmm as fmm'
            ])->get();

        return $passenger;
    }

    public function setPassengerUser($request){
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = 'passenger';
        $user->password = Hash::make($request->password);
        $user->save();

        return $user;
    }

    public function setPassenger($request, $user){
        $passenger = new Passenger();
        $passenger->user_id = $user->id;
        $passenger->company_id = $request->company_id;
        $passenger->phone = $request->phone;
        $passenger->job_title = $request->job_title;
        $passenger->gender = $request->gender;
        $passenger->fmm = $request->fmm;
        $passenger->save();

        return $passenger;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Passenger  $passenger
     * @return \Illuminate\Http\Response
     */
    public function show(Passenger $passenger)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Passenger  $passenger
     * @return \Illuminate\Http\Response
     */
    public function edit(Passenger $passenger)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatePassengerRequest  $request
     * @param  \App\Models\Passenger  $passenger
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePassengerRequest $request, Passenger $passenger)
    {
        $this->updatePassengerUser($request, $passenger);
        $this->updatePassenger($request, $passenger);
        $passenger = $this->getPassengerInfo($passenger->id);
        return response($passenger, 200);
    }

    public function updatePassengerUser($request, $passenger){
        $user = $passenger->user;
        $user->name = $request->name;
        $user->email = $request->email;
        if($request->has('password')){
            $user->password = Hash::make($request->password);
        }
        $user->save();
    }

    public function updatePassenger($request, $passenger){
        $passenger->company_id = $request->company_id;
        $passenger->phone = $request->phone;
        $passenger->gender = $request->gender;
        $passenger->job_title = $request->job_title;
        $passenger->fmm = $request->fmm;

        $passenger->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Passenger  $passenger
     * @return \Illuminate\Http\Response
     */
    public function destroy(Passenger $passenger)
    {
        //
    }
}
