<?php

namespace App\Http\Controllers;

use App\Models\PassengerAddress;
use App\Http\Requests\StorePassengerAddressRequest;
use App\Http\Requests\UpdatePassengerAddressRequest;

class PassengerAddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorePassengerAddressRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePassengerAddressRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PassengerAddress  $passengerAddress
     * @return \Illuminate\Http\Response
     */
    public function show(PassengerAddress $passengerAddress)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PassengerAddress  $passengerAddress
     * @return \Illuminate\Http\Response
     */
    public function edit(PassengerAddress $passengerAddress)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatePassengerAddressRequest  $request
     * @param  \App\Models\PassengerAddress  $passengerAddress
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePassengerAddressRequest $request, PassengerAddress $passengerAddress)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PassengerAddress  $passengerAddress
     * @return \Illuminate\Http\Response
     */
    public function destroy(PassengerAddress $passengerAddress)
    {
        //
    }
}
