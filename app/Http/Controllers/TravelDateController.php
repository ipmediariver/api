<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTravelDateRequest;
use App\Http\Requests\UpdateTravelDateRequest;
use App\Models\Travel;
use App\Models\TravelDate;
use App\Models\Vehicle;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class TravelDateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $travelDates = $this->getTravelDates(); 
        return response($travelDates, 200);
    }

    public function nextTravelAvailable()
    {
        $travelDate = $this->getTravelDates()->take(1);
        return response($travelDate, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTravelDateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTravelDateRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TravelDate  $travelDate
     * @return \Illuminate\Http\Response
     */
    public function show(TravelDate $travelDate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TravelDate  $travelDate
     * @return \Illuminate\Http\Response
     */
    public function edit(TravelDate $travelDate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTravelDateRequest  $request
     * @param  \App\Models\TravelDate  $travelDate
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTravelDateRequest $request, TravelDate $travelDate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TravelDate  $travelDate
     * @return \Illuminate\Http\Response
     */
    public function destroy(TravelDate $travelDate)
    {
        
    }

    public function getTravelDates(){
        $today      = Carbon::now();
        $travelDates = DB::table('travel_dates')
            ->where('travel_dates.date', '>', $today)
            ->leftJoin('vehicles', 'travel_dates.vehicle_id', 'vehicles.id')
            ->orderBy('travel_dates.date', 'asc')
            ->orderBy('travel_dates.start_at', 'asc')
            ->select([
                'travel_dates.id as id',
                'travel_dates.date as date',
                'travel_dates.start_at as hour',
                'travel_dates.departure as departure',
                'travel_dates.destination as destination',
                'vehicles.id as vehicle_id',
                'vehicles.model as vehicle_model',
                'vehicles.brand as vehicle_brand',
                'vehicles.year as vehicle_year',
                'vehicles.plates as vehicle_plates',
            ])
            ->get()
            ->map(function ($travelDate) {
                $travel =  TravelDate::find($travelDate->id);
                $pickedUpLocations = $travel->pickedUpLocations->map(function($location){
                    return [
                        'id' => $location->id,
                        'address' => $location->address->fullAddress
                    ];
                });
                if($travelDate->vehicle_id){
                    $vehicle                    = Vehicle::find($travelDate->vehicle_id);
                    $passengers                 = Travel::where('travel_date_id', $travelDate->id)->whereIn('status_id', [0,1])->count();
                    $seats                      = $vehicle->seats;
                    $availableSeats             = ($seats - $passengers);
                    $travelDate->availableSeats = "{$availableSeats}/{$seats}";
                }
                $travelDate->date           = Carbon::parse($travelDate->date)->format('d M, Y');
                $travelDate->hour           = Carbon::parse($travelDate->hour)->format('h:i a');
                $travelDate->locations = $pickedUpLocations;
                return $travelDate;
            });
        return $travelDates;
    }
}
