<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\TravelDate;
use Illuminate\Http\Request;
use Carbon\Carbon;

class TravelDateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $travelDates = $this->filterTravelDates();
        return view('admin.travel-dates.index', compact('travelDates'));
    }

    public function filterTravelDates(){
        $travelDates = \DB::table('travel_dates')
            ->select([
                'travel_dates.id as id',
                'travel_dates.date as date',
                'travel_dates.start_at as hour',
                'travel_dates.departure as departure',
                'travel_dates.destination as destination',
            ])
            ->get()
            ->map(function($travelDate){
                $travelDate->date = Carbon::parse($travelDate->date)->format('d M, Y');
                $travelDate->hour = Carbon::parse($travelDate->hour)->format('h:i a');
                return $travelDate;
            });
        return $travelDates;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.travel-dates.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $travelDate = new TravelDate();
        $travelDate->date = Carbon::parse($request->date);
        $travelDate->start_at = Carbon::parse($request->hour);
        $travelDate->departure = $request->departure;
        $travelDate->destination = $request->destination;
        $travelDate->save();

        return redirect()->route('show-travel-date', $travelDate)->with('status', 'Travel date created with success.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $travelDate = TravelDate::find($id);
        return view('admin.travel-dates.show', compact('travelDate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $travelDate = TravelDate::find($id);
        $travelDate->date = Carbon::parse($request->date);
        $travelDate->start_at = Carbon::parse($request->hour);
        $travelDate->departure = $request->departure;
        $travelDate->destination = $request->destination;
        $travelDate->save();

        return redirect()->route('show-travel-date', $travelDate)->with('status', 'Travel date was updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $travelDate = TravelDate::find($id);
        
        $travelDate->pickedUpLocations()->delete();
        $travelDate->passengers()->delete();
        $travelDate->vehicle->delete();
        $travelDate->driver->delete();

        $travelDate->delete();
        return redirect()->route('travel-dates-list')->with('status', 'Travel date deleted.');
    }

    public function addVehicleAndDriver(Request $request, $travelDateId){
        $travelDate = TravelDate::find($travelDateId);
        $travelDate->vehicle_id = $request->vehicle_id;
        $travelDate->driver_id = $request->driver_id;
        $travelDate->save();

        return redirect()->back()->with('status', 'Information updated.');
    }
}
