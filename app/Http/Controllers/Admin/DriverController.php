<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Driver;
use App\Models\User;
use Illuminate\Http\Request;

class DriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $drivers = Driver::get()->load('user')->sortBy('user.name');
        return view('admin.drivers.index', compact('drivers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.drivers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $driverUser = $this->setDriverUser($request, new User());
        $driver     = $this->setDriver($request, $driverUser, new Driver());
        return redirect()->route('show-driver', $driver)->with('status', 'Driver created with success.');
    }

    public function setDriverUser($request, $user){
        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = 'driver';
        if($request->has('password')){
            $user->password = \Hash::make($request->password);
        }
        $user->save();
        return $user;
    }

    public function setDriver($request, $user, $driver){
        $driver->user_id = $user->id;
        $driver->license_num = $request->license_num;
        $driver->gender = $request->gender;
        $driver->save();

        return $driver;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $driver = Driver::find($id);
        return view('admin.drivers.show', compact('driver'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $driver = Driver::find($id);
        return view('admin.drivers.edit', $driver);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $driver     = Driver::find($id);
        $driverUser = $this->setDriverUser($request, $driver->user);
        $driver     = $this->setDriver($request, $driverUser, $driver);
        return redirect()->back()->with('status', 'Driver information updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $driver = Driver::find($id);
        $driver->delete();
        return redirect()->route('drivers-list')->with('status', 'Driver deleted.');
    }
}
