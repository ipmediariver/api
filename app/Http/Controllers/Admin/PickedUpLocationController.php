<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\PickedUpLocation;
use App\Models\TravelDate;
use Illuminate\Http\Request;

class PickedUpLocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($travelDateId)
    {
        $travelDate = TravelDate::find($travelDateId);
        return view('admin.travel-dates.picked-up-locations.create', compact('travelDate'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $travelDate)
    {

        $travelDate = TravelDate::find($travelDate);

        if($request->has('address_id')){
            
            $pickedUpLocation = new PickedUpLocation();
            $pickedUpLocation->address_id = $request->address_id;
            $pickedUpLocation->travel_date_id = $travelDate->id;
            $pickedUpLocation->save();

            return redirect()->route('show-travel-date', $travelDate)->with('status', 'New picked up location added.');
        }

        $address = $this->createNewAddress($request);
        $pickedUpLocation = new PickedUpLocation();
        $pickedUpLocation->address_id = $address->id;
        $pickedUpLocation->travel_date_id = $travelDate->id;
        $pickedUpLocation->save();

        return redirect()->route('show-travel-date', $travelDate)->with('status', 'New picked up location added.');
    }

    public function createNewAddress($request){
        $address = new Address();
        $address->street = $request->street;
        $address->city = $request->city;
        $address->state = $request->state;
        $address->country = $request->country;
        $address->zip_code = $request->zip;
        $address->alias = "{$request->street}, {$request->city} {$request->state}, {$request->country}, {$request->zip}";
        $address->save();

        return $address;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($travelDateId, $pickedUpLocationId)
    {
        
        $pickedUpLocation = PickedUpLocation::find($pickedUpLocationId);

        $pickedUpLocation->delete();

        return redirect()->back()->with('status', 'Picked up location deleted.');

    }
}
