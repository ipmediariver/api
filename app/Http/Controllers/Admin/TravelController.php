<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Admin\AppController;
use App\Http\Controllers\Controller;
use App\Models\Travel;
use Illuminate\Http\Request;
use Validator;

class TravelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     
        $travels = app(AppController::class)
                    ->filterTravels()
                    ->orderBy('travel_dates.date', 'desc')
                    ->orderBy('travel_dates.start_at', 'desc')
                    ->get();
                    
        $travels = app(AppController::class)->resetTravelsFields($travels);

        return view('admin.travels.index', compact('travels'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($travel)
    {

        $travel = app(AppController::class)->getTravelData($travel);
        return view('admin.travels.show', compact('travel'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validate = Validator::make(
            $request->all(), 
            ['status_id' => 'required|integer|in:0,1,2,3']
        )->validate();

        $travel = Travel::find($id);
        $travel->status_id = $request->status_id;
        $travel->save();

        return redirect()->back()->with('status', 'Travel updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
