<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Travel;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AppController extends Controller
{
    public function dashboard(){
        $currentTravels = $this->getCurrentTravels()->toArray();
        return view('admin.dashboard', compact('currentTravels'));
    }

    public function getCurrentTravels(){
        $today = Carbon::now();
        $travels = $this->filterTravels()
                        ->where('travel_dates.date', '>=', $today)
                        ->orderBy('travel_dates.date', 'asc')
                        ->orderBy('travel_dates.start_at', 'asc')
                        ->get();
        $travels = $this->resetTravelsFields($travels);
        return $travels;
    }

    public function resetTravelsFields($travels){
        return $travels->map(function($travel){
            $travel->day = Carbon::parse($travel->day)->format('Y/m/d');
            $travel->hour = Carbon::parse($travel->hour)->format('H:i');
            $travel->status = $this->formatTravelStatus($travel->status);
            return $travel;
        });
    }

    public function filterTravels(){
        $travels = \DB::table('travel')
            ->leftJoin('travel_dates', 'travel.travel_date_id', 'travel_dates.id')
            ->leftJoin('passengers', 'travel.passenger_id', 'passengers.id')
            ->leftJoin('users', 'passengers.user_id', 'users.id')
            ->select([
                'travel.id as id',
                'users.name as passenger',
                'travel_dates.date as day',
                'travel_dates.start_at as hour',
                'travel_dates.departure as departure',
                'travel_dates.destination as destination',
                'travel.status_id as status'
            ]);
        return $travels;
    }

    public function getTravelData($travelId){
        $travel = \DB::table('travel')
            ->where('travel.id', $travelId)
            ->leftJoin('travel_dates', 'travel.travel_date_id', 'travel_dates.id')
            ->leftJoin('picked_up_locations', 'travel.picked_up_location_id', 'picked_up_locations.id')
            ->leftJoin('addresses', 'picked_up_locations.address_id', 'addresses.id')
            ->leftJoin('passengers', 'travel.passenger_id', 'passengers.id')
            ->leftJoin('users', 'passengers.user_id', 'users.id')
            ->select([
                'travel.id as id',
                'travel.created_at as createdAt',
                'users.name as passenger',
                'travel_dates.id as travel_date_id',
                'travel_dates.date as day',
                'travel_dates.start_at as hour',
                'travel_dates.departure as departure',
                'travel_dates.destination as destination',
                'addresses.alias as pickedUp',
                'travel.status_id as status'
            ])
            ->get()
            ->map(function($travel){
                $travel->createdAt = Carbon::parse($travel->createdAt)->diffForHumans();
                $travel->day = Carbon::parse($travel->day)->format('d M, Y');
                $travel->hour = Carbon::parse($travel->hour)->format('h:i a');
                return $travel;
            })
            ->first();
        return $travel;
    }

    public function formatTravelStatus($status){
        switch ($status) {
            case 0:
                return "Pending approval";
                break;
            case 1:
                return "Approved";
                break;
            case 2:
                return "Cancelled";
                break;
            case 3:
                return "Rejected";
                break;
            
            default:
                return "Pending approval";
                break;
        }
    }

}
