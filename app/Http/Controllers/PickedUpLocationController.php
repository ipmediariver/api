<?php

namespace App\Http\Controllers;

use App\Models\PickedUpLocation;
use App\Http\Requests\StorePickedUpLocationRequest;
use App\Http\Requests\UpdatePickedUpLocationRequest;

class PickedUpLocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorePickedUpLocationRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePickedUpLocationRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PickedUpLocation  $pickedUpLocation
     * @return \Illuminate\Http\Response
     */
    public function show(PickedUpLocation $pickedUpLocation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PickedUpLocation  $pickedUpLocation
     * @return \Illuminate\Http\Response
     */
    public function edit(PickedUpLocation $pickedUpLocation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdatePickedUpLocationRequest  $request
     * @param  \App\Models\PickedUpLocation  $pickedUpLocation
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePickedUpLocationRequest $request, PickedUpLocation $pickedUpLocation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PickedUpLocation  $pickedUpLocation
     * @return \Illuminate\Http\Response
     */
    public function destroy(PickedUpLocation $pickedUpLocation)
    {
        //
    }
}
