<?php
namespace App\Http\Controllers;

use App\Http\Requests\StoreTravelRequest;
use App\Http\Requests\UpdateTravelRequest;
use App\Models\Driver;
use App\Models\PickedUpLocation;
use App\Models\Travel;
use App\Models\TravelDate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class TravelController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin')->only('update', 'delete');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user    = $request->user();
        $travels = $this->getTravelsByRole($user);
        $travels->map(function ($travel) {
            info(collect($travel)->toArray());
            $location            = PickedUpLocation::find($travel->location_id);
            $travelDate          = TravelDate::find($travel->travel_date_id);
            $travel->status      = $this->getTravelStatus($travel);
            $travel->driver_name = $this->getDriverName($travel);
            $travel->departure   = $travelDate->departure;
            $travel->destination = $travelDate->destination;
            $travel->pickedUp    = $location->address->fullAddress;
            $travel->date = Carbon::parse($travel->date)->format('d M, Y');
            $travel->start_at = Carbon::parse($travel->start_at)->format('h:i a');
            return $travel;
        });
        return response($travels, 200);
    }
    public function getTravelsByRole($user)
    {
        $travels = DB::table('travel')
            ->join('passengers', 'travel.passenger_id', 'passengers.id')
            ->join('users', 'passengers.user_id', 'users.id')
            ->join('travel_dates', 'travel.travel_date_id', 'travel_dates.id')
            ->join('vehicles', 'travel_dates.vehicle_id', 'vehicles.id')
            ->join('drivers', 'travel_dates.driver_id', 'drivers.id')
            ->select([
                'travel.id as id',
                'travel_dates.id as travel_date_id',
                'users.name as passenger_name',
                'travel_dates.date as date',
                'travel_dates.start_at as start_at',
                'travel.status_id as status_id',
                'vehicles.type as vehicle_type',
                'vehicles.brand as vehicle_brand',
                'vehicles.model as vehicle_model',
                'vehicles.year as vehicle_year',
                'vehicles.plates as vehicle_plates',
                'vehicles.color as vehicle_color',
                'drivers.id as driver_id',
                'travel.picked_up_location_id as location_id'
            ]);
        if ($user->role == 'driver') {
            $driver = $user->driver;
            $travels->where('travel_dates.driver_id', $driver->id);
        }
        if ($user->role == 'passenger') {
            $passenger = $user->passenger;
            $travels->where('travel.passenger_id', $passenger->id);
        }
        return $travels->orderBy('travel_dates.date', 'asc')->get();
    }
    public function getDriverName($travel)
    {
        $driver = Driver::find($travel->driver_id);
        return $driver ? $driver->user->name : null;
    }
    public function getTravelStatus($travel)
    {
        switch ($travel->status_id) {
            case 0:
                $status = "Pending approval";
                break;
            case 1:
                $status = "Approved";
                break;
            case 2:
                $status = "Cancelled";
                break;
            case 3:
                $status = "Rejected";
                break;
            default:
                $status = "Pending approval";
                break;
        }
        return $status;
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTravelRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTravelRequest $request)
    {
        //
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Travel  $travel
     * @return \Illuminate\Http\Response
     */
    public function show(Travel $travel)
    {
        $travelData = $this->getTravelData($travel->id);
        return response($travelData, 200);
    }
    public function getTravelData($id)
    {
        $travel = DB::table('travel')
            ->where('travel.id', $id)
            ->join('passengers', 'travel.passenger_id', 'passengers.id')
            ->join('users', 'passengers.user_id', 'users.id')
            ->join('travel_dates', 'travel.travel_date_id', 'travel_dates.id')
            ->join('vehicles', 'travel_dates.vehicle_id', 'vehicles.id')
            ->join('drivers', 'travel_dates.driver_id', 'drivers.id')
            ->select([
                'travel.id as id',
                'travel_dates.id as travel_date_id',
                'users.name as passenger_name',
                'travel_dates.date as date',
                'travel_dates.start_at as start_at',
                'travel.status_id as status_id',
                'vehicles.type as vehicle_type',
                'vehicles.brand as vehicle_brand',
                'vehicles.model as vehicle_model',
                'vehicles.year as vehicle_year',
                'vehicles.plates as vehicle_plates',
                'vehicles.color as vehicle_color',
                'drivers.id as driver_id',
                'travel.picked_up_location_id as location_id'
            ])->get();
        return $travel;
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Travel  $travel
     * @return \Illuminate\Http\Response
     */
    public function edit(Travel $travel)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTravelRequest  $request
     * @param  \App\Models\Travel  $travel
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTravelRequest $request, Travel $travel)
    {
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Travel  $travel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Travel $travel)
    {
        $travel->status_id = 2;
        $travel->save();
        $response = ["message" => "Travel cancelled"];
        return response($response, 200);
    }
    public function newPassengerReservation(Request $request)
    {
        $user      = $request->user();
        $passenger = $user->passenger;
        $role      = $user->role;
        if ($role != 'passenger') {
            $response = ['message' => 'Only passengers can reserve a travel'];
            return response($response, 402);
        }
        if(!$this->validateIfSeatsAvailable($request)){
            $response = ['message' => 'There are no seats available for this travel'];
            return response($response, 402);
        }
        $travel                        = new Travel();
        $travel->company_id            = $passenger->company_id;
        $travel->passenger_id          = $passenger->id;
        $travel->travel_date_id        = $request->travel_date_id;
        $travel->picked_up_location_id = $request->picked_up_location_id;
        $travel->status_id             = 0;
        $travel->save();
        $response = $this->getTravelData($travel->id);
        return response($response, 200);
    }

    public function validateIfSeatsAvailable($request){
        $travelDate = TravelDate::find($request->travel_date_id);
        $vehicle = $travelDate->vehicle;
        $seats = $vehicle->seats;
        $passengers = count($travelDate->passengers);
        if($passengers >= $seats){
            return false;
        }
        return true;
    }

    public function cancelReservation(Request $request){
        $user = $request->user();
        $reservation = Travel::find($request->travel_id);
    }
}
