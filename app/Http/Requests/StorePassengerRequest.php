<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePassengerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'fmm' => 'required|in:0,1',
            'gender' => 'required|in:male,female',
            'job_title' => 'sometimes|string|nullable',
            'company_id' => 'required|exists:companies,id',
            'password' => 'required|min:6|max:15'
        ];
    }
}
