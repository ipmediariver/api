<?php

namespace App\View\Components\Admin;

use Illuminate\View\Component;

class VehicleForm extends Component
{

    public $vehicle;
    public $title;
    public $route;
    public $method;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($method = null, $route = null, $title = null, $vehicle = null)
    {
        $this->vehicle = $vehicle;
        $this->title = $title;
        $this->method = $method;
        $this->route = $route;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.admin.vehicle-form');
    }
}
