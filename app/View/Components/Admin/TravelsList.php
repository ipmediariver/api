<?php

namespace App\View\Components\Admin;

use Illuminate\View\Component;

class TravelsList extends Component
{

    public $travels;
    public $title;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($travels, $title)
    {
        $this->travels = $travels;
        $this->title = $title;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.admin.travels-list');
    }
}
