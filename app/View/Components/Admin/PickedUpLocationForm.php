<?php

namespace App\View\Components\Admin;

use App\Models\Address;
use Illuminate\View\Component;

class PickedUpLocationForm extends Component
{

    public $travelDate;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($travelDate)
    {
        $this->travelDate = $travelDate;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.admin.picked-up-location-form');
    }


    public function addresses(){
        return Address::orderBy('alias')->get();
    }

}
