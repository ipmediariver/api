<?php

namespace App\View\Components\Admin;

use App\Models\Driver;
use App\Models\Vehicle;
use Illuminate\View\Component;

class TravelDateDetails extends Component
{

    public $method;
    public $travelDate;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($travelDate = null, $method = null)
    {
        $this->method = $method;
        $this->travelDate = $travelDate;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.admin.travel-date-details');
    }


    public function vehicles(){
        $vehicles = Vehicle::orderBy('brand')->get();
        return $vehicles;
    }

    public function drivers(){
        $drivers = Driver::all();
        return $drivers;
    }

}
