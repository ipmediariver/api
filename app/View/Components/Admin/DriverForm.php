<?php

namespace App\View\Components\Admin;

use Illuminate\View\Component;

class DriverForm extends Component
{

    public $driver;
    public $title;
    public $method;
    public $route;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($driver = null, $title = null, $method = 'POST', $route = null)
    {
        $this->driver = $driver;
        $this->title = $title;
        $this->method = $method;
        $this->route = $route;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.admin.driver-form');
    }
}
