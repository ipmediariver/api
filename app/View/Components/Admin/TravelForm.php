<?php

namespace App\View\Components\Admin;

use App\Models\Travel;
use Illuminate\View\Component;

class TravelForm extends Component
{

    public $travel;
    public $route;
    public $method;
    public $day;
    public $hour;
    public $passenger;
    public $departure;
    public $destination;
    public $pickedUp;
    public $status;
    public $createdAt;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($route = null, $method = 'POST', $travel = null)
    {
        $this->route       = $route;
        $this->method      = $method;
        $this->travel      = $travel;
        $this->day         = $travel ? $travel->day : null;
        $this->hour        = $travel ? $travel->hour : null;
        $this->passenger   = $travel ? $travel->passenger : null;
        $this->departure   = $travel ? $travel->departure : null;
        $this->destination = $travel ? $travel->destination : null;
        $this->pickedUp    = $travel ? $travel->pickedUp : null;
        $this->status      = $travel ? $travel->status : null;
        $this->createdAt   = $travel ? $travel->createdAt : null;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.admin.travel-form');
    }
}
