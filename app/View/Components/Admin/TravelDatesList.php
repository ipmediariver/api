<?php

namespace App\View\Components\Admin;

use Illuminate\View\Component;

class TravelDatesList extends Component
{


    public $title;
    public $travelDates;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($title, $travelDates)
    {
        $this->title = $title;
        $this->travelDates = $travelDates;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.admin.travel-dates-list');
    }
}
