<?php

namespace App\View\Components\Admin;

use Illuminate\View\Component;

class UserForm extends Component
{

    public $user;
    public $title;
    public $method;
    public $route;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($user = null, $title = null, $method = 'POST', $route = null)
    {
        $this->user   = $user;
        $this->title  = $title;
        $this->method = $method;
        $this->route  = $route;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.admin.user-form');
    }
}
