<?php

namespace App\View\Components\Admin;

use Illuminate\View\Component;

class VehiclesList extends Component
{


    public $title;
    public $vehicles;


    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($vehicles = null, $title = null)
    {
        $this->vehicles = $vehicles;
        $this->title = $title;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.admin.vehicles-list');
    }
}
