<?php

namespace App\View\Components\Admin;

use Illuminate\View\Component;

class UsersList extends Component
{


    public $users;
    public $title;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($users = null, $title = null)
    {
        $this->users = $users;
        $this->title = $title;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.admin.users-list');
    }
}
