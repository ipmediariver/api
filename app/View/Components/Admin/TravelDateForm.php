<?php
namespace App\View\Components\Admin;
use App\Models\Address;
use Carbon\Carbon;
use Illuminate\View\Component;
class TravelDateForm extends Component
{

    public $title;
    public $method;
    public $route;
    public $travelDate;
    public $date;
    public $hour;
    public $departure;
    public $destination;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($title = null, $travelDate = null, $route = null, $method = 'POST')
    {
        $this->title = $title;
        $this->method = $method;
        $this->route = $route;
        $this->travelDate = $travelDate;
        $this->date        = $travelDate ? Carbon::parse($travelDate->date)->format('Y-m-d') : null;
        $this->hour        = $travelDate ? Carbon::parse($travelDate->start_at)->format('H:i') : null;
        $this->departure   = $travelDate ? $travelDate->departure : null;
        $this->destination = $travelDate ? $travelDate->destination : null;
    }
    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.admin.travel-date-form');
    }
    public function addresses()
    {
        return Address::orderBy('alias')->all();
    }
}