<?php

namespace App\View\Components\Admin;

use Illuminate\View\Component;

class DriversList extends Component
{


    public $drivers;
    public $title;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($drivers, $title)
    {
        $this->drivers = $drivers;
        $this->title = $title;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.admin.drivers-list');
    }
}
