<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory;

    public function getFullAddressAttribute(){
        return "{$this->street}, {$this->city} {$this->state} {$this->country}, {$this->zip_code}";
    }

}
