<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Travel extends Model
{
    use HasFactory;

    public function company(){
        return $this->belongsTo(Company::class);
    }

    public function passenger(){
        return $this->belongsTo(Passenger::class);
    }

    public function date(){
        return $this->belongsTo(TravelDate::class, 'travel_date_id', 'id');
    }

    public function pickedUpLocation(){
        return $this->belongsTo(PickedUpLocation::class, 'picked_up_location_id', 'id');
    }

    public function getStatusAttribute(){

        switch ($this->status_id) {
            case 0:
                $status = "Pending approval";
                break;
            case 1:
                $status = "Approved";
                break;
            case 2:
                $status = "Cancelled";
                break;
            case 3:
                $status = "Rejected";
                break;
            
            default:
                $status = "Pending approval";
                break;
        }

        return $status;

    }

    public function getDayAttribute(){
        return Carbon::parse($this->date->date)->format('d M, Y');
    }

    public function getHourAttribute(){
        return Carbon::parse($this->date->start_at)->format('h:i a');
    }

}
