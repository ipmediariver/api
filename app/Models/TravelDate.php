<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TravelDate extends Model
{
    use HasFactory;

    public function vehicle(){
        return $this->belongsTo(Vehicle::class);
    }

    public function driver(){
        return $this->belongsTo(Driver::class);
    }

    public function passengers(){
        return $this->hasMany(Travel::class, 'travel_date_id', 'id');
    }

    public function pickedUpLocations(){
        return $this->hasMany(PickedUpLocation::class, 'travel_date_id', 'id');
    }

}
