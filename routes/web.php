<?php

use App\Http\Controllers\Admin\AppController;
use App\Http\Controllers\Admin\DriverController;
use App\Http\Controllers\Admin\PickedUpLocationController;
use App\Http\Controllers\Admin\TravelController;
use App\Http\Controllers\Admin\TravelDateController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\VehicleController;
use Illuminate\Support\Facades\Route;

Route::redirect('/', 'login');
Route::get('access-denied', function(){
    return view('access-denied');
})->name('access-denied');

Route::group(['prefix' => 'dashboard', 'middleware' => ['auth', 'admin']], function(){
    Route::get('/', [AppController::class, 'dashboard'])->name('dashboard');
    Route::resource('travels', TravelController::class, [
        'names' => [
            'index' => 'travels-list',
            'show' => 'show-travel',
            'edit' => 'edit-travel',
            'update' => 'update-travel',
            'create' => 'create-travel',
            'destroy' => 'delete-travel'
        ]
    ]);
    Route::resource('travel-dates', TravelDateController::class, [
        'names' => [
            'index' => 'travel-dates-list',
            'show' => 'show-travel-date',
            'store' => 'store-travel-date',
            'edit' => 'edit-travel-date',
            'update' => 'update-travel-date',
            'create' => 'create-travel-date',
            'destroy' => 'delete-travel-date'
        ]
    ]);
    Route::post('add-vehicle-and-driver-to-travel-date/{travelDateId}', [
        TravelDateController::class, 'addVehicleAndDriver'
    ])->name('add-vehicle-and-driver-to-travel-date');
    Route::resource('travel-dates.picked-up-locations', PickedUpLocationController::class, [
        'names' => [
            'index' => 'picked-up-locations-list',
            'show' => 'show-picked-up-location',
            'store' => 'store-picked-up-location',
            'edit' => 'edit-picked-up-location',
            'update' => 'update-picked-up-location',
            'create' => 'create-picked-up-location',
            'destroy' => 'delete-picked-up-location'
        ]
    ]);
    Route::resource('vehicles', VehicleController::class, [
        'names' => [
            'index' => 'vehicles-list',
            'show' => 'show-vehicle',
            'store' => 'store-vehicle',
            'edit' => 'edit-vehicle',
            'update' => 'update-vehicle',
            'create' => 'create-vehicle',
            'destroy' => 'delete-vehicle'
        ]
    ]);
    Route::resource('drivers', DriverController::class, [
        'names' => [
            'index' => 'drivers-list',
            'show' => 'show-driver',
            'store' => 'store-driver',
            'edit' => 'edit-driver',
            'update' => 'update-driver',
            'create' => 'create-driver',
            'destroy' => 'delete-driver'
        ]
    ]);
    Route::resource('users', UserController::class, [
        'names' => [
            'index' => 'users-list',
            'show' => 'show-user',
            'store' => 'store-user',
            'edit' => 'edit-user',
            'update' => 'update-user',
            'create' => 'create-user',
            'destroy' => 'delete-user'
        ]
    ]);
});

require __DIR__.'/auth.php';
