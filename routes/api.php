<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DriverController;
use App\Http\Controllers\TravelController;
use App\Http\Controllers\PassengerController;
use App\Http\Controllers\TravelDateController;
use App\Http\Controllers\Auth\AuthApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['middleware' => 'cors'], function(){

    Route::post('/login', [AuthApiController::class, 'login']);

    Route::group(['middleware' => 'auth:api'], function(){
        Route::post('/logout', [AuthApiController::class, 'logout']);
        Route::get('user', [AuthApiController::class, 'user']);
        Route::resource('travels', TravelController::class);
        Route::post('travel-reservation', [TravelController::class, 'reservation']);
        Route::resource('drivers', DriverController::class);
        Route::resource('passengers', PassengerController::class);
        Route::resource('travel-dates', TravelDateController::class);
        Route::get('next-travel-date-available', [TravelDateController::class, 'nextTravelAvailable']);
        Route::post('new-passenger-reservation', [TravelController::class, 'newPassengerReservation']);
        Route::delete('cancel-reservation/{travel}', [TravelController::class, 'destroy']);
    });

});

